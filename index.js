// Mock Database
let posts = [];

let count = 1

// Add Post Data
// No prob if you put querySelector inside a var or not
document.querySelector("#form-add-post").addEventListener('submit', (e) => {

	e.preventDefault();

	posts.push ({

		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	count++;

	showPosts(posts)
	alert('Successfully added.')
})

const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `

			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}"> ${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>

		`	
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries
}

// Edit Post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}


// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {

			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPosts(posts);
			alert("Successfully updated");

			break;
		}
	}
})

// Delete Post
/*
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	console.log(title)
	console.log()*/

/*const deletePost = (id) => {
	document.querySelector('#div-post-entries').innerHTML = id
	console.log(id)
	id = ''
	console.log(id)
}

document.querySelector('#div-post-entries').removeEventListener('submit', (deletePost) => {
	deletePost.preventDefault()
})
*/


let deletePost = (id) => {
	console.log(posts)
	posts = posts.filter((post) => {
		if(post.id.toString() !== id){
			console.log(post)
			return post
		}
	})
	document.querySelector(`#post-${id}`).remove()
	console.log(posts)
	alert('Successfully deleted')

}